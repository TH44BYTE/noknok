package haibotech.noknok;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    private static final String url = "http://192.168.42.79:1234/Grocerease/index.php"; //Xampp Server address

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Volley to connect to shopping database
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Can't connect to server", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(stringRequest);
    }

    private void showJSON(String json) {
        ParseJSON pj = new ParseJSON(json);
        pj.parseJSON();

        //Open fragment that displays shelf list from database
        Bundle shelf_bundle = new Bundle();
        shelf_bundle.putParcelableArrayList("db_items",ParseJSON.dbList);

        ShelfFragment shelf = new ShelfFragment();
        shelf.setArguments(shelf_bundle);
        getFragmentManager().beginTransaction().add(R.id.container, shelf).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        }
        else {
            if (count == 1) {
                finish();
            } else {
                getFragmentManager().popBackStack();
            }
        }
    }

    public void Checkout(View view) {
        Toast.makeText(this,"Coming Soon...", Toast.LENGTH_LONG).show();

    }
}