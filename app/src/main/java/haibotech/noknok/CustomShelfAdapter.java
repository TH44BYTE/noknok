package haibotech.noknok;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by NCRMO_000 on 03/11/2016.
 */
public class CustomShelfAdapter extends RecyclerView.Adapter<CustomShelfAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private final OnItemClickListener listener;

    private final ArrayList<Item> itemList;

    public CustomShelfAdapter(ArrayList<Item> items, OnItemClickListener listener) {
        this.itemList = items;
        //this.prices = itemPrices;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewItemName, textViewPrice;
        private AppCompatImageButton addToCartButton;

        public ViewHolder(View listViewItem) {
            super(listViewItem);
            textViewItemName = (TextView) listViewItem.findViewById(R.id.itemNameTextView);
            textViewPrice = (TextView) listViewItem.findViewById(R.id.priceTextView);

            addToCartButton = (AppCompatImageButton) listViewItem.findViewById(R.id.addToCartButton);
        }

        public void bind(final int position, final OnItemClickListener  listener) {
            addToCartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position);
                }
            });
        }
    }

    @Override
    public CustomShelfAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shelf_card,parent,false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewItemName.setText(itemList.get(position).getName());
        String price = String.format("R%1$.2f",itemList.get(position).getPrice());
        holder.textViewPrice.setText(price);
            holder.bind(position, listener);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }



}
