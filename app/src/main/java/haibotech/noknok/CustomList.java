package haibotech.noknok;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by NCRMO_000 on 27/10/2016.
 */

/*
private static class CustomList extends RecyclerView.Adapter<CustomViewHolder> {

    private String[] itemNames;
    private String[] prices;
    private Activity context;

    public LayoutInflater inflater;

    public CustomList(Activity context, String[] itemNames, String[] prices) {
        super(context, R.layout.shelf_card, itemNames);
        this.context = context;
        this.itemNames = itemNames;
        this.prices = prices;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewItemName, textViewPrice;
        public NumberPicker quantityPicker;
        public AppCompatImageButton addToCartButton;


        public ViewHolder(View itemView) {
            super(itemView);
            textViewItemName = (TextView) itemView.findViewById(R.id.itemNameTextView);
            textViewPrice = (TextView) itemView.findViewById(R.id.priceTextView);
            quantityPicker = (NumberPicker) itemView.findViewById(R.id.quantityPicker)
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.shelf_card, null, true);
        TextView textViewItemName = (TextView) listViewItem.findViewById(R.id.itemNameTextView);
        TextView textViewPrice = (TextView) listViewItem.findViewById(R.id.priceTextView);

        textViewItemName.setText(itemNames[position]);
        textViewPrice.setText(prices[position]);

        NumberPicker quantityPicker = (NumberPicker) listViewItem.findViewById(R.id.quantityPicker);
        quantityPicker.computeScroll();
        quantityPicker.setValue(0);
        quantityPicker.setMinValue(0);
        quantityPicker.setMaxValue(10);
        quantityPicker.setWrapSelectorWheel(false);

        AppCompatImageButton addToCartButton = (AppCompatImageButton) listViewItem.findViewById(R.id.addToCartButton);
        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardView vwParentRow = (CardView) v.getParent();
                TextView child = (TextView)vwParentRow.getChildAt(0);
                Toast.makeText(v.getContext(), child.getText() + " added to Cart", Toast.LENGTH_LONG).show();
            }
        });

        return listViewItem;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return 0;
    }
}
*/

/*public class ItemHolder extends RecyclerView.ViewHolder {

    private String[] itemNames;
    private String[] prices;

    private final TextView textViewItemName, textViewPrice;
    private final NumberPicker quantityPicker;
    private final AppCompatImageButton addToCartButton;

    private Context context;

    public ItemHolder(Context context, View itemView) {
        super (itemView);
        this.context = context;

        this.textViewItemName = (TextView) itemView.findViewById(R.id.itemNameTextView);
        this.textViewPrice = (TextView) itemView.findViewById(R.id.priceTextView);
        this.quantityPicker = (NumberPicker) itemView.findViewById(R.id.quantityPicker);
        this.addToCartButton = (AppCompatImageButton) itemView.findViewById(R.id.addToCartButton);

    public void bindItems(){
    }
}

}*/