package haibotech.noknok;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Created by NCRMO_000 on 27/10/2016.
 */

public class ParseJSON {
    public static ArrayList<Item> dbList = new ArrayList<>();

    public static final String JSON_ARRAY = "shelf";
    public static final String KEY_ITEM = "ItemName";
    public static final String KEY_PRICE = "Price";

    private JSONArray shelf = null;

    private String json;

    public ParseJSON(String json) {
        this.json = json;
    }

    protected void parseJSON() {
        JSONObject jso = null;
        try{
            jso = new JSONObject(json);
            shelf = jso.getJSONArray(JSON_ARRAY);

            for (int i = 0; i < shelf.length(); i++) {
                Item jsonItem = new Item();
                JSONObject jo = shelf.getJSONObject(i);
                jsonItem.setName(jo.getString(KEY_ITEM));
                String tempStr = jo.getString(KEY_PRICE);
                tempStr = tempStr.replace("$","");
                Double temp = Double.parseDouble(tempStr);
                jsonItem.setPrice(round(temp,2));
                dbList.add(jsonItem);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
