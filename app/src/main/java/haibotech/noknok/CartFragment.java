package haibotech.noknok;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by NCRMO_000 on 08/11/2016.
 */

public class CartFragment extends Fragment {
    private RecyclerView.LayoutManager cartLayoutManager;
    private RecyclerView cartList;
    private RecyclerView.Adapter cartAdapter;
    private TextView total;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.cartview, container, false);

        ArrayList<Item> cartItems = getArguments().getParcelableArrayList("cartItems");

        cartList = (RecyclerView) rootView.findViewById(R.id.cartView);
        cartList.setHasFixedSize(true);

        cartLayoutManager = new LinearLayoutManager(getActivity());
        cartList.setLayoutManager(cartLayoutManager);

        cartAdapter = new CustomCartAdapter(cartItems);
        cartList.setAdapter(cartAdapter);
        total = (TextView) rootView.findViewById(R.id.TotalText);

        updateTotal(total);

        ((CustomCartAdapter) cartList.getAdapter()).setListener(new CustomCartAdapter.CartListener() {
            @Override
            public void onItemRemoved() {
                updateTotal(total);
            }

            @Override
            public void onQuantityChanged() {
                updateTotal(total);
            }
        });

        return rootView;
    }

    private void updateTotal(TextView total) {
        double price =  ((CustomCartAdapter) cartAdapter).getTotal();
        String totalPrice = String.format("Total: R%1$.2f",price);
        total.setText(totalPrice);

    }


}
