package haibotech.noknok;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NCRMO_000 on 06/12/2016.
 */

public class Item implements Parcelable {
    private String name;
    private double price;
    private int quantity;

    public Item() {
        this.quantity = 1;
    }

    protected Item(Parcel in) {
        name = in.readString();
        price = in.readDouble();
        quantity = in.readInt();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] { this.name, String.valueOf(this.price), String.valueOf(this.quantity)
        });
    }

    /*private void readFromParcel (Parcel in) {
        name = in.readString();
        price = in.readDouble();
        quantity = in.readInt();
    }*/
}
