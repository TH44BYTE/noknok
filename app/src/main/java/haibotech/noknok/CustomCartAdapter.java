package haibotech.noknok;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by NCRMO_000 on 03/11/2016.
 */
public class CustomCartAdapter extends RecyclerView.Adapter<CustomCartAdapter.ViewHolder> {

    private final ArrayList<Item> cartItems;

    public interface CartListener {
        void onItemRemoved();
        void onQuantityChanged();
    }

    private CartListener mListener;

    public CustomCartAdapter(ArrayList<Item> items) {
        this.cartItems = items;
        this.mListener = null;
    }

    public void setListener(CartListener listener) {
        this.mListener = listener;
    }

    public void deleteItem(int position) {
        cartItems.remove(position);
        notifyItemRemoved(position);
        mListener.onItemRemoved();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, NumberPicker.OnValueChangeListener {

        private TextView textViewItemName, textViewPrice;
        private NumberPicker quantityPicker;
        private AppCompatImageButton removeItem;

        public ViewHolder(View listViewItem) {
            super(listViewItem);
            textViewItemName = (TextView) listViewItem.findViewById(R.id.itemNameTextView);
            textViewPrice = (TextView) listViewItem.findViewById(R.id.priceTextView);

            quantityPicker = (NumberPicker) listViewItem.findViewById(R.id.quantityPicker);
            quantityPicker.computeScroll();
            quantityPicker.setMinValue(1);
            quantityPicker.setMaxValue(10);
            quantityPicker.setValue(1);
            quantityPicker.setWrapSelectorWheel(false);
            quantityPicker.setOnValueChangedListener(this);
            removeItem = (AppCompatImageButton) listViewItem.findViewById(R.id.removeButton);
            removeItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            deleteItem(getAdapterPosition());
        }

        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            cartItems.get(getAdapterPosition()).setQuantity(newVal);
            mListener.onQuantityChanged();
        }
    }

    @Override
    public CustomCartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewItemName.setText(cartItems.get(position).getName());
        String price = String.format("R%1$.2f",cartItems.get(position).getPrice());
        holder.textViewPrice.setText(price);
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public double getTotal() {
        double total = 0.00;
           for (int i = 0; i < getItemCount(); i++) {
                total = total + cartItems.get(i).getPrice()*cartItems.get(i).getQuantity();
            }
            return total;
        }
}



