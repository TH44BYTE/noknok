package haibotech.noknok;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by NCRMO_000 on 09/11/2016.
 */

public class ShelfFragment extends Fragment implements View.OnClickListener{
    private RecyclerView.LayoutManager shelfLayoutManager;
    private RecyclerView shelfList;
    private RecyclerView.Adapter shelfAdapter;

    public ArrayList<Item> cartItems = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.shelfview, container, false);
        final ArrayList<Item> db_list = getArguments().getParcelableArrayList("db_items");
        shelfList = (RecyclerView) rootView.findViewById(R.id.listView);
        shelfList.setHasFixedSize(true);

        shelfLayoutManager = new LinearLayoutManager(getActivity());
        shelfList.setLayoutManager(shelfLayoutManager);

        shelfAdapter = new CustomShelfAdapter(db_list, new CustomShelfAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(int position) {
                Toast.makeText(getActivity(), db_list.get(position).getName() + " added to cart", Toast.LENGTH_SHORT).show();
                boolean notAdded = true;
                for (int i = 0; i < cartItems.size(); i++) {
                    if (db_list.get(position).getName() == cartItems.get(i).getName()) {
                        notAdded = false;
                        Toast.makeText(getActivity(), db_list.get(position).getName() + " already added at position", Toast.LENGTH_SHORT).show();
                    }
                }

                if(notAdded) {
                    cartItems.add(db_list.get(position));
                }
            }
        });
        shelfList.setAdapter(shelfAdapter);

        Button checkoutButton = (Button) rootView.findViewById(R.id.checkoutButton);
        checkoutButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        Bundle cart_bundle = new Bundle();
        cart_bundle.putParcelableArrayList("cartItems", cartItems);

        CartFragment cart = new CartFragment();
        cart.setArguments(cart_bundle);
        getFragmentManager().beginTransaction().add(R.id.container, cart).addToBackStack(null).commit();
    }


}

